<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('renren.php');

$renren_t=isset($_SESSION['renren_t'])?$_SESSION['renren_t']:'';

//检查是否已登录
if($renren_t!=''){
	$renren=new renrenPHP($renren_k, $renren_s, $renren_t);

	//获取登录用户信息
	$result=$renren->me();
	var_dump($result);

	/**
	//access token到期后使用refresh token刷新access token
	$result=$renren->access_token_refresh($_SESSION['renren_r']);
	var_dump($result);
	**/

	/**
	//更新状态
	$result=$renren->setStatus('状态内容');
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：获取登录用户信息
	$result=$renren->api('user/login/get', array(), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$renren=new renrenPHP($renren_k, $renren_s);
	$login_url=$renren->login_url($callback_url, $scope);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
